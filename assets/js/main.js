  jQuery(document).ready(function() {
      $(".header .icon-menu").click(function() {
         $(".menu-mobile").addClass("open");
         $(".overlay").addClass("open");
      });
      $(".overlay").click(function() {
         $(".menu-mobile").removeClass("open");
         $(this).removeClass("open");
      });
      Fancybox.bind("[data-fancybox]", {
         on: {
         load: (fancybox, slide) => {
            console.log(`#${slide.index} slide is loaded!`);
            console.log(
               `This slide is selected: ${fancybox.getSlide().index === slide.index}`
            );
         },
         },
      });
 });